/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Vector;
//import javax.resource.cci.ResultSet;

/**
 *
 * @author Wera
 */
public class ReadDb {
     private Vector<Customer> List = new Vector<Customer>();
  protected ResultSet result;
  protected Connection connection = null;
  public void initialize() throws IOException{
      
      Properties defaultProps = new Properties();
        defaultProps.load(ReadDb.class.getResourceAsStream("/test.properties"));
  
    String url=defaultProps.getProperty("url");
    String pasport =defaultProps.getProperty("pasvord") ;
    String driver =defaultProps.getProperty("driver") ;
    String parol = defaultProps.getProperty("parol");

	  try{
              System.out.println("Start");
             //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		  Class.forName(driver);//("org.apache.derby.jdbc.ClientDriver");
          }
          catch(Exception e) { 
              System.out.println("Class def not found: " + e);
          }
		 // String url = "jdbc:derby://localhost:1527/sample";
		 // String username = "1234";
		 // String password = "1234";
          try {        
		  connection = DriverManager.getConnection(url,pasport,parol);//, username, password);  
                  
	  }
	  catch (Throwable theException){theException.printStackTrace();
          System.out.println("Error");
          }		  
  }
  public void execute(String mySQL) throws IOException{
	  try{
		  Statement statement = connection.createStatement(); 
		  result = statement.executeQuery(mySQL);
			//  ("select sName, City, status"+
			//   " from s where status > 10");
		  boolean h;
		  while(result.next()){	
			  /*if (result.getInt(7) == 0) h= false;
			  else h = true;*/											 
			  List.addElement(new Customer
				  (result.getInt("CUSTOMER_ID"),result.getString("NAME")
				   ));
		  }	
		  result.close();
		  statement.close();
		  connection.close();			  
	  }
	  catch (Throwable theException){
              theException.printStackTrace();
          }	 
  } 
  public int getSize(){
	  return List.size();	  
  }
  public Customer getCustomer(int i){
     	  try{ return List.elementAt(i);}
	  catch (ArrayIndexOutOfBoundsException e) {return null;}
  }
 
    /** Creates a new instance of sDB */
    public ReadDb() {
    }



}
